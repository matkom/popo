use popo::error::PopoError;
use popo::lexer::tokens::{Token, TokenKind};
use popo::ast::*;
use popo::ast::visitor::*;
use popo::interpreter::{
    Interpreter,
    types::{Value, PopoFunction},
    environment::Environment
};

use std::{cell::RefCell, rc::Rc};

#[test]
fn visit_lit_num_test() {
    match Interpreter::new().visit_lit(&Lit::Num(420.0)).unwrap() {
        Value::Num(val) => assert_eq!(420.0, val),
        _ => assert!(false),
    }
}

#[test]
fn visit_lit_str_test() {
    match Interpreter::new().visit_lit(&Lit::Str("420".to_string())).unwrap() {
        Value::Str(val) => assert_eq!("420", val),
        _ => assert!(false),
    }
}

#[test]
fn visit_lit_bool_test() {
    match Interpreter::new().visit_lit(&Lit::Bool(true)).unwrap() {
        Value::Bool(val) => assert!(val),
        _ => assert!(false),
    }
}

#[test]
fn visit_lit_nil_test() {
    match Interpreter::new().visit_lit(&Lit::Nil).unwrap() {
        Value::Nil => assert!(true),
        _ => assert!(false),
    }
}

#[test]
fn visit_var_expr_bool_test() -> Result<(), PopoError> {
    let test_bool_var = Token::new(1, TokenKind::Identifier, "test_bool_var".to_string(), 1);
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(&test_bool_var.value, Value::Bool(true));

    match Interpreter::new_with_env(env.clone()).visit_var_expr(&test_bool_var)? {
        Value::Bool(val) => assert!(val),
        _ => assert!(false),
    }

    Ok(())
}

#[test]
fn visit_var_expr_num_test() -> Result<(), PopoError> {
    let test_num_var = Token::new(1, TokenKind::Identifier, "test_num_var".to_string(), 1);
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(&test_num_var.value, Value::Num(420.0));

    match Interpreter::new_with_env(env.clone()).visit_var_expr(&test_num_var)? {
        Value::Num(val) => assert_eq!(420.0, val),
        _ => assert!(false),
    }

    Ok(())
}

#[test]
fn visit_var_expr_str_test() -> Result<(), PopoError> {
    let test_str_var = Token::new(1, TokenKind::Identifier, "test_str_var".to_string(), 1);
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(&test_str_var.value, Value::Str("420".to_string()));

    match Interpreter::new_with_env(env.clone()).visit_var_expr(&test_str_var)? {
        Value::Str(val) => assert_eq!("420", val),
        _ => assert!(false),
    }

    Ok(())
}

#[test]
fn visit_var_expr_nil_test() -> Result<(), PopoError> {
    let test_nil_var = Token::new(1, TokenKind::Identifier, "test_nil_var".to_string(), 1);
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(&test_nil_var.value, Value::Nil);

    match Interpreter::new_with_env(env.clone()).visit_var_expr(&test_nil_var)? {
        Value::Nil => assert!(true),
        _ => assert!(false),
    }

    Ok(())
}

#[test]
fn visit_var_expr_callable_test() -> Result<(), PopoError> {
    let test_callable_var = Token::new(1, TokenKind::Identifier, "test_callable_var".to_string(), 1);
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(
        &test_callable_var.value,
        Value::Callable(Rc::new(PopoFunction::new(
            FnStmt {
                name: Token::new(1, TokenKind::Identifier, "test_func_var".to_string(), 1),
                params: Vec::new(),
                body: Vec::new()
            },
            env.clone()
        )))
    );

    match Interpreter::new_with_env(env.clone()).visit_var_expr(&test_callable_var)? {
        Value::Callable(_) => assert!(true),
        _ => assert!(false),
    }

    Ok(())
}

#[test]
fn visit_call_expr_test() -> Result<(), PopoError> {
    let env = Rc::new(RefCell::new(Environment::global()));
    env.borrow_mut().define(
        "test_func",
        Value::Callable(Rc::new(PopoFunction::new(
            FnStmt {
                name: Token::new(1, TokenKind::Identifier, "test_func".to_string(), 1),
                params: Vec::new(),
                body: vec![
                    Stmt::ReturnStmt(ReturnStmt{
                        keyword: Token::new(1, TokenKind::Identifier, "return".to_string(), 1),
                        value: Some(Expr::Lit(Lit::Num(420.0)))
                    })
                ],
            },
            env.clone()
        ))),
    );

    let call_node = Call {
        callee: Expr::Var(Token::new(1,
            TokenKind::Identifier,
            "test_func".to_string(),
            1,
        )),
        args: Vec::new(),
        paren: Token::new(1, TokenKind::LParenthesis, "(".to_string(), 1),
    };

    let result = Interpreter::new_with_env(env.clone()).visit_call_expr(&call_node)?;
    match result {
        Value::Num(num) => assert!(num == 420.0),
        _ => assert!(false)
    }

    Ok(())
}

// TODO Test all visitor methods

//#[test]
//fn visit_binary_expr_test() {
//    todo!()
//}
//
//#[test]
//fn visit_unary_expr_test() {
//    todo!()
//}
//
//#[test]
//fn visit_grouping_expr_test() {
//    todo!()
//}
//
//#[test]
//fn visit_expr_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_if_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_while_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_let_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_fn_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_block_stmt_test() {
//    todo!()
//}
//
//#[test]
//fn visit_return_stmt_test() {
//    todo!()
//}
