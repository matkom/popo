use popo::lexer::{Tokenizer, tokens::TokenKind};

#[test]
fn num_lit_int_test() {
    let mut tokenizer = Tokenizer::new(String::from("420"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Number, tokens[0].kind);
    assert_eq!("420", tokens[0].value);
}

#[test]
fn num_lit_float_test() {
    let mut tokenizer = Tokenizer::new(String::from("420.69"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Number, tokens[0].kind);
    assert_eq!("420.69", tokens[0].value);
}

#[test]
fn str_literal_test() {
    let mut tokenizer = Tokenizer::new(String::from("\"string\""));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Str, tokens[0].kind);
    assert_eq!("\"string\"", tokens[0].value);
}

#[test]
fn semicolon_test() {
    let mut tokenizer = Tokenizer::new(String::from(";"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Semicolon, tokens[0].kind);
    assert_eq!(";", tokens[0].value);
}

#[test]
fn whitespace_test() {
    let mut tokenizer = Tokenizer::new(String::from("   "));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Eof, tokens[0].kind);
}

#[test]
fn line_comment_test() {
    let mut tokenizer = Tokenizer::new(String::from("#This is a line comment"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Eof, tokens[0].kind);
}

#[test]
fn open_block_test() {
    let mut tokenizer = Tokenizer::new(String::from("{"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::LCurlyBrace, tokens[0].kind);
    assert_eq!("{", tokens[0].value);
}

#[test]
fn close_block_test() {
    let mut tokenizer = Tokenizer::new(String::from("}"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::RCurlyBrace, tokens[0].kind);
    assert_eq!("}", tokens[0].value);
}

#[test]
fn add_op_test() {
    let mut tokenizer = Tokenizer::new(String::from("+"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Plus, tokens[0].kind);
    assert_eq!("+", tokens[0].value);
}

#[test]
fn sub_op_test() {
    let mut tokenizer = Tokenizer::new(String::from("-"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Minus, tokens[0].kind);
    assert_eq!("-", tokens[0].value);
}

#[test]
fn mult_op_test() {
    let mut tokenizer = Tokenizer::new(String::from("*"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Star, tokens[0].kind);
    assert_eq!("*", tokens[0].value);
}

#[test]
fn div_op_test() {
    let mut tokenizer = Tokenizer::new(String::from("/"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Slash, tokens[0].kind);
    assert_eq!("/", tokens[0].value);
}

#[test]
fn open_parenthesis_test() {
    let mut tokenizer = Tokenizer::new(String::from("("));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::LParenthesis, tokens[0].kind);
    assert_eq!("(", tokens[0].value);
}

#[test]
fn close_parenthesis_test() {
    let mut tokenizer = Tokenizer::new(String::from(")"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::RParenthesis, tokens[0].kind);
    assert_eq!(")", tokens[0].value);
}

#[test]
fn identifier_test() {
    let mut tokenizer = Tokenizer::new(String::from("identifier"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Identifier, tokens[0].kind);
    assert_eq!("identifier", tokens[0].value);
}

#[test]
fn fn_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("fn"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Fn, tokens[0].kind);
    assert_eq!("fn", tokens[0].value);
}

#[test]
fn true_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("true"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::True, tokens[0].kind);
    assert_eq!("true", tokens[0].value);
}

#[test]
fn false_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("false"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::False, tokens[0].kind);
    assert_eq!("false", tokens[0].value);
}

#[test]
fn if_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("if"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::If, tokens[0].kind);
    assert_eq!("if", tokens[0].value);
}

#[test]
fn else_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("else"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Else, tokens[0].kind);
    assert_eq!("else", tokens[0].value);
}


#[test]
fn and_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("and"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::And, tokens[0].kind);
    assert_eq!("and", tokens[0].value);
}

#[test]
fn or_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("or"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Or, tokens[0].kind);
    assert_eq!("or", tokens[0].value);
}

#[test]
fn for_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("for"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::For, tokens[0].kind);
    assert_eq!("for", tokens[0].value);
}

#[test]
fn while_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("while"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::While, tokens[0].kind);
    assert_eq!("while", tokens[0].value);
}

#[test]
fn let_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("let"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Let, tokens[0].kind);
    assert_eq!("let", tokens[0].value);
}

#[test]
fn return_keyword_test() {
    let mut tokenizer = Tokenizer::new(String::from("return"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Return, tokens[0].kind);
    assert_eq!("return", tokens[0].value);
}

#[test]
fn bang_test() {
    let mut tokenizer = Tokenizer::new(String::from("!"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Bang, tokens[0].kind);
    assert_eq!("!", tokens[0].value);
}

#[test]
fn bang_eq_test() {
    let mut tokenizer = Tokenizer::new(String::from("!="));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::BangEq, tokens[0].kind);
    assert_eq!("!=", tokens[0].value);
}

#[test]
fn eq_test() {
    let mut tokenizer = Tokenizer::new(String::from("="));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Assign, tokens[0].kind);
    assert_eq!("=", tokens[0].value);
}

#[test]
fn eq_eq_test() {
    let mut tokenizer = Tokenizer::new(String::from("=="));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Eq, tokens[0].kind);
    assert_eq!("==", tokens[0].value);
}

#[test]
fn greater_test() {
    let mut tokenizer = Tokenizer::new(String::from(">"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Greater, tokens[0].kind);
    assert_eq!(">", tokens[0].value);
}

#[test]
fn greater_eq_test() {
    let mut tokenizer = Tokenizer::new(String::from(">="));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::GreaterEq, tokens[0].kind);
    assert_eq!(">=", tokens[0].value);
}

#[test]
fn less_test() {
    let mut tokenizer = Tokenizer::new(String::from("<"));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::Less, tokens[0].kind);
    assert_eq!("<", tokens[0].value);
}

#[test]
fn less_eq_test() {
    let mut tokenizer = Tokenizer::new(String::from("<="));

    let (tokens, errors) = &tokenizer.get_all_tokens();

    assert!(errors.is_empty());
    assert_eq!(TokenKind::LessEq, tokens[0].kind);
    assert_eq!("<=", tokens[0].value);
}

#[test]
fn unknown_token_test() {
    let mut tokenizer = Tokenizer::new(String::from("%"));

    let (_, errors) = tokenizer.get_all_tokens();

    assert_eq!("Unknown token %", errors[0].msg);
    assert_eq!(1, errors[0].line);
}
