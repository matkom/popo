use popo::error::PopoError;
use popo::parser::Parser;
use popo::lexer::Tokenizer;
use popo::lexer::tokens::{Token, TokenKind};
use popo::ast::*;

#[test]
fn num_lit_int_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![Stmt::ExprStmt(Expr::Lit(Lit::Num(420.0)))];

    let (tokens, errors) = Tokenizer::new(String::from("420;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn num_lit_float_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![Stmt::ExprStmt(Expr::Lit(Lit::Num(420.69)))];

    let (tokens, errors) = Tokenizer::new(String::from("420.69;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn str_lit_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![Stmt::ExprStmt(Expr::Lit(Lit::Str(String::from("string"))))];

    let (tokens, errors) = Tokenizer::new(String::from("\"string\";")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn multiple_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(Expr::Lit(Lit::Num(420.0))),
        Stmt::ExprStmt(Expr::Lit(Lit::Num(420.69))),
        Stmt::ExprStmt(Expr::Lit(Lit::Str(String::from("string")))),
    ];

    let (tokens, errors) = Tokenizer::new(String::from("420;420.69;\"string\";")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn wrong_expr_test() -> Result<(), ()> {
    let (tokens, errors) = Tokenizer::new(String::from("420")).get_all_tokens();

    match Parser::new(tokens).parse() {
        Ok(_) => Err(()),
        Err(err) => {
            assert!(errors.is_empty());
            assert_eq!(1, err.len());
            assert_eq!("Unexpected end of input, expected: ;", err[0].msg);
            assert_eq!(1, err[0].line);
            Ok(())
        }
    }
}

#[test]
fn empty_block_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![Stmt::BlockStmt(Vec::new())];

    let (tokens, errors) = Tokenizer::new(String::from("{}")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn nested_empty_blocks_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::BlockStmt(vec![
            Stmt::BlockStmt(vec![
                Stmt::BlockStmt(vec![])
            ])
        ])
    ];

    let (tokens, errors) = Tokenizer::new(String::from("{{{}}}")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn non_empty_block_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::BlockStmt(vec![
            Stmt::ExprStmt(Expr::Lit(Lit::Num(420.0))),
            Stmt::ExprStmt(Expr::Lit(Lit::Num(420.69))),
            Stmt::ExprStmt(Expr::Lit(Lit::Str(String::from("string")))),
        ])
    ];

    let (tokens, errors) = Tokenizer::new(String::from("{420;420.69;\"string\";}")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn nested_non_empty_blocks_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::BlockStmt(vec![
            Stmt::ExprStmt(Expr::Lit(Lit::Str(String::from("string")))),
            Stmt::BlockStmt(vec![
                Stmt::ExprStmt(Expr::Lit(Lit::Num(420.69))),
                Stmt::BlockStmt(vec![
                    Stmt::ExprStmt(Expr::Lit(Lit::Num(420.0)))
                ])
            ])
        ])
    ];

    let (tokens, errors) = Tokenizer::new(String::from("{\"string\";{420.69;{420;}}}")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn unclosed_block_err_test() -> Result<(), ()> {
    let (tokens, errors) = Tokenizer::new(String::from("{")).get_all_tokens();

    match Parser::new(tokens).parse() {
        Ok(_) => Err(()),
        Err(err) => {
            assert!(errors.is_empty());
            assert_eq!("Unexpected end of input, expected: }", err[0].msg);
            assert_eq!(1, err[0].line);
            Ok(())
        }
    }
}

#[test]
fn unopened_block_err_test() -> Result<(), ()> {
    let (tokens, errors) = Tokenizer::new(String::from("}")).get_all_tokens();

    match Parser::new(tokens).parse() {
        Ok(_) => Err(()),
        Err(err) => {
            assert!(errors.is_empty());
            assert_eq!("Unknown literal: }", err[0].msg);
            assert_eq!(1, err[0].line);
            Ok(())
        }
    }
}

#[test]
fn add_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(1, TokenKind::Plus, "+".to_string(), 1),
                Expr::Lit(Lit::Num(2.0)),
                Expr::Lit(Lit::Num(2.0))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("2+2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn subtract_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(1, TokenKind::Minus, "-".to_string(), 1),
                Expr::Lit(Lit::Num(2.0)),
                Expr::Lit(Lit::Num(2.0))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("2-2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn multiply_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(1, TokenKind::Star, "*".to_string(), 1),
                Expr::Lit(Lit::Num(2.0)),
                Expr::Lit(Lit::Num(2.0))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("2*2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn divide_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(1, TokenKind::Slash, "/".to_string(), 1),
                Expr::Lit(Lit::Num(2.0)),
                Expr::Lit(Lit::Num(2.0))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("2/2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn multiple_binary_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(3, TokenKind::Minus, "-".to_string(), 1),
                Expr::Binary(Box::new(BinaryExpr::new(
                    Token::new(1, TokenKind::Plus, "+".to_string(), 1),
                    Expr::Lit(Lit::Num(2.0)),
                    Expr::Lit(Lit::Num(2.0))
                ))),
                Expr::Binary(Box::new(BinaryExpr::new(
                    Token::new(7, TokenKind::Slash, "/".to_string(), 1),
                    Expr::Binary(Box::new(BinaryExpr::new(
                        Token::new(5, TokenKind::Star, "*".to_string(), 1),
                        Expr::Lit(Lit::Num(2.0)),
                        Expr::Lit(Lit::Num(2.0)),
                    ))),
                    Expr::Lit(Lit::Num(2.0))
                )))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("2+2-2*2/2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

#[test]
fn parenthesis_binary_expr_test() -> Result<(), Vec<PopoError>> {
    let expected = vec![
        Stmt::ExprStmt(
            Expr::Binary(Box::new(BinaryExpr::new(
                Token::new(5, TokenKind::Star, "*".to_string(), 1),
                Expr::Grouping(Box::new(Expr::Binary(Box::new(BinaryExpr::new(
                    Token::new(2, TokenKind::Plus, "+".to_string(), 1),
                    Expr::Lit(Lit::Num(2.0)),
                    Expr::Lit(Lit::Num(2.0))
                ))))),
                Expr::Lit(Lit::Num(2.0))
            )))
        )
    ];

    let (tokens, errors) = Tokenizer::new(String::from("(2+2)*2;")).get_all_tokens();
    let result = Parser::new(tokens).parse()?;

    assert!(errors.is_empty());
    assert_eq!(expected, result);
    Ok(())
}

// TODO Write tests for new grammar
