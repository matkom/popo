use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
    pub id: usize,
    pub kind: TokenKind,
    pub value: String,
    pub line: usize
}

impl Token {
    pub fn new(
        id: usize,
        kind: TokenKind,
        value: String,
        line: usize,
    ) -> Self {
        Self {
            id,
            kind,
            value,
            line,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TokenKind {

    /* Literals */
    Number,
    Str,
    Identifier,
    Nil,

    /* Keywords */
    Fn,
    True,
    False,
    If,
    Else,
    And,
    Or,
    For,
    While,
    Let,
    Return,

    /* Single char tokens */
    LCurlyBrace,
    RCurlyBrace,
    LParenthesis,
    RParenthesis,
    LSqrBracket,
    RSqrBracket,
    Semicolon,
    Comma,

    /* Math operators */
    Plus,
    Minus,
    Star,
    Slash,

    /* Comparison and assignment operators */
    Assign,
    Eq,
    Bang,
    BangEq,
    Greater,
    GreaterEq,
    Less,
    LessEq,

    /* Special tokens */
    Empty,
    Newline,
    Eof,
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            TokenKind::Number => f.write_str("Number"),
            TokenKind::Str => f.write_str("String"),
            TokenKind::Identifier => f.write_str("Identifier"),
            TokenKind::Nil => f.write_str("nil"),
            TokenKind::Fn => f.write_str("fn"),
            TokenKind::True => f.write_str("true"),
            TokenKind::False => f.write_str("false"),
            TokenKind::If => f.write_str("if"),
            TokenKind::Else => f.write_str("else"),
            TokenKind::And => f.write_str("and"),
            TokenKind::Or => f.write_str("or"),
            TokenKind::For => f.write_str("for"),
            TokenKind::While => f.write_str("while"),
            TokenKind::Let => f.write_str("let"),
            TokenKind::Return => f.write_str("return"),
            TokenKind::Semicolon => f.write_str(";"),
            TokenKind::LCurlyBrace => f.write_str("{"),
            TokenKind::RCurlyBrace => f.write_str("}"),
            TokenKind::LParenthesis => f.write_str("("),
            TokenKind::RParenthesis => f.write_str(")"),
            TokenKind::LSqrBracket => f.write_str("["),
            TokenKind::RSqrBracket => f.write_str("]"),
            TokenKind::Comma => f.write_str(","),
            TokenKind::Plus => f.write_str("+"),
            TokenKind::Minus => f.write_str("-"),
            TokenKind::Star => f.write_str("*"),
            TokenKind::Slash => f.write_str("/"),
            TokenKind::Bang => f.write_str("!"),
            TokenKind::BangEq => f.write_str("!="),
            TokenKind::Assign => f.write_str("="),
            TokenKind::Eq => f.write_str("=="),
            TokenKind::Greater => f.write_str(">"),
            TokenKind::GreaterEq => f.write_str(">="),
            TokenKind::Less => f.write_str("<"),
            TokenKind::LessEq => f.write_str("<="),
            TokenKind::Newline => f.write_str("\n"),
            TokenKind::Empty => f.write_str("Empty"),
            TokenKind::Eof => f.write_str("EOF"),
        }
    }
}
