use std::collections::HashMap;
use crate::error::PopoError;
use crate::ast::*;
use crate::ast::visitor::*;
use crate::interpreter::Interpreter;
use crate::lexer::tokens::Token;

pub struct Resolver<'a> {
    interpreter: &'a mut Interpreter,
    scopes: Vec<HashMap<String, bool>>,
    curr_fn: FnType
}

#[derive(PartialEq, Clone, Copy)]
enum FnType {
    None,
    Fn
}

impl<'a> Resolver<'a> {
    pub fn new(interpreter: &'a mut Interpreter) -> Self {
        Self {
            interpreter,
            scopes: Vec::new(),
            curr_fn: FnType::None
        }
    }

    pub fn resolve(&mut self, stmts: &Vec<Stmt>) -> Result<(), PopoError> {
        for stmt in stmts {
            walk_stmt(self, stmt)?;
        }
        Ok(())
    }

    fn begin_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    fn end_scope(&mut self) {
        self.scopes.pop();
    }

    fn declare(&mut self, name: &Token) -> Result<(), PopoError> {
        if let Some(scope) = self.scopes.last_mut() {
            if scope.contains_key(&name.value) {
                return Err(PopoError::new(
                    name.line,
                    "Return statements are not allowed in a top-level code".to_string()
                ))
            }
            scope.insert(name.value.clone(), false);
        }
        Ok(())
    }

    fn define(&mut self, name: &Token) {
        if let Some(scope) = self.scopes.last_mut() {
            scope.insert(name.value.clone(), true);
        }
    }

    fn resolve_local(&mut self, name: &Token) {
        for i in (0..self.scopes.len()).rev() {
            if self.scopes[i].contains_key(&name.value) {
                self.interpreter.resolve(name, self.scopes.len() - i - 1);
                return;
            }
        }
    }

    fn resolve_fn(&mut self, fn_stmt: &FnStmt, fn_type: FnType) -> Result<(), PopoError> {
        let enclosing_fn = self.curr_fn;
        self.curr_fn = fn_type;
        self.begin_scope();
        for param in &fn_stmt.params {
            self.declare(param)?;
            self.define(param);
        }
        self.resolve(&fn_stmt.body)?;
        self.end_scope();
        self.curr_fn = enclosing_fn;
        Ok(())
    }
}

impl<'a> StmtVisitor<(), PopoError> for Resolver<'a> {
    fn visit_block_stmt(&mut self, stmts: &Vec<Stmt>) -> Result<(), PopoError> {
        self.begin_scope();
        self.resolve(stmts)?;
        self.end_scope();
        Ok(())
    }

    fn visit_expr_stmt(&mut self, expr: &Expr) -> Result<(), PopoError> {
        walk_expr(self, expr)
    }

    fn visit_fn_stmt(&mut self, fn_stmt: &FnStmt) -> Result<(), PopoError> {
        let name = &fn_stmt.name;
        self.declare(name)?;
        self.define(name);
        self.resolve_fn(fn_stmt, FnType::Fn)
    }

    fn visit_if_stmt(&mut self, if_stmt: &IfStmt) -> Result<(), PopoError> {
        walk_expr(self, &if_stmt.condition)?;
        walk_stmt(self, &if_stmt.then_branch)?;
        if let Some(else_branch) = &if_stmt.else_branch {
            walk_stmt(self, else_branch)?;
        }
        Ok(())
    }

    fn visit_let_stmt(&mut self, let_stmt: &LetStmt) -> Result<(), PopoError> {
        self.declare(&let_stmt.var_name)?;
        if let Some(initializer) = &let_stmt.init {
            walk_expr(self, initializer)?;
        }
        self.define(&let_stmt.var_name);
        Ok(())
    }

    fn visit_return_stmt(&mut self, return_stmt: &ReturnStmt) -> Result<(), PopoError> {
        if self.curr_fn == FnType::None {
            return Err(PopoError::new(
                return_stmt.keyword.line,
                "Cannot return from top level code".to_string()
            ));
        }

        if let Some(value) = &return_stmt.value {
            walk_expr(self, value)?;
        }
        Ok(())
    }

    fn visit_while_stmt(&mut self, while_stmt: &WhileStmt) -> Result<(), PopoError> {
        walk_expr(self, &while_stmt.condition)?;
        walk_stmt(self, &while_stmt.body)
    }
}

impl<'a> ExprVisitor<(), PopoError> for Resolver<'a> {
    fn visit_binary_expr(&mut self, expr: &BinaryExpr) -> Result<(), PopoError> {
        walk_expr(self, &expr.left)?;
        walk_expr(self, &expr.right)
    }

    fn visit_call_expr(&mut self, expr: &Call) -> Result<(), PopoError> {
        walk_expr(self, &expr.callee)?;
        for arg in &expr.args {
            walk_expr(self, arg)?;
        }
        Ok(())
    }

    fn visit_grouping_expr(&mut self, expr: &Expr) -> Result<(), PopoError> {
        walk_expr(self, expr)
    }

    fn visit_lit(&mut self, _: &Lit) -> Result<(), PopoError> {
        Ok(())
    }

    fn visit_unary_expr(&mut self, expr: &UnaryExpr) -> Result<(), PopoError> {
        walk_expr(self, &expr.right)
    }

    fn visit_var_expr(&mut self, name: &Token) -> Result<(), PopoError> {
        if self.scopes.is_empty() {
            return Ok(());
        }

        let last_idx = self.scopes.len() - 1;
        if self.scopes[last_idx].get(&name.value) == Some(&false) {
            return Err(PopoError::new(
                name.line,
                "Cannot read local variable in its own initializer".to_string()
            ));
        }

        self.resolve_local(name);
        Ok(())
    }

    fn visit_assign_expr(&mut self, expr: &AssignExpr) -> Result<(), PopoError> {
        match &expr.left {
            Expr::Var(var_name) => {
                walk_expr(self, &expr.right)?;
                self.resolve_local(&var_name);
                Ok(())
            }
            Expr::Member(member_expr) =>{
                walk_expr(self, &expr.right)?;
                walk_expr(self, &member_expr.idx)?;
                walk_expr(self, &member_expr.object)
            }
            _ => Err(PopoError::new(
                expr.token.line,
                format!("Cannot assign to: {}", expr.left)
            ))
        }
    }

    fn visit_list_expr(&mut self, list: &Vec<Expr>) -> Result<(), PopoError> {
        for item in list {
            if let Expr::Assign(assign_expr) = item {
                return Err(PopoError::new(
                    assign_expr.token.line,
                    "Cannot assign variables inside a list body".to_string()
                ));
            }
            walk_expr(self, item)?;
        }
        Ok(())
    }

    fn visit_member_expr(&mut self, expr: &MemberExpr) -> Result<(), PopoError> {
        walk_expr(self, &expr.idx)?;
        walk_expr(self, &expr.object)
    }
}
