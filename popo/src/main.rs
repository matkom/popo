use std::fs;
use std::process::exit;
use popo::error::{report_errors, report_error};
use popo::interpreter::Interpreter;
use popo::parser::Parser;
use popo::lexer::Tokenizer;
use popo::resolver::Resolver;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("USGAE:\n{} [<filename>.popo]", env!("CARGO_PKG_NAME"));
        exit(1);
    }

    let file = &args[1];
    let program = fs::read_to_string(file).unwrap_or_else(|err| {
        panic!("[ERROR] Failed to read file: {}!\nError message: {}", file, err)
    });

    let (tokens, lexing_errs) = Tokenizer::new(program).get_all_tokens();
    let lexing_err = !lexing_errs.is_empty();

    match Parser::new(tokens).parse() {
        Ok(_) if lexing_err => report_errors(lexing_errs),
        Ok(ast) => {
            let mut interpreter = Interpreter::new();
            let mut resolver = Resolver::new(&mut interpreter);
            match resolver.resolve(&ast) {
                Ok(()) => interpreter.interpret(&ast),
                Err(err) => report_error(err)
            }
        }
        Err(errs) => {
            report_errors(lexing_errs);
            report_errors(errs);
        }
    }
}
