use crate::ast::*;

pub trait StmtVisitor<T, E> {
    fn visit_expr_stmt(&mut self, expr: &Expr) -> Result<T, E>;
    fn visit_if_stmt(&mut self, if_stmt: &IfStmt) -> Result<T, E>;
    fn visit_while_stmt(&mut self, while_stmt: &WhileStmt) -> Result<T, E>;
    fn visit_let_stmt(&mut self, let_stmt: &LetStmt) -> Result<T, E>;
    fn visit_block_stmt(&mut self, stmts: &Vec<Stmt>) -> Result<T, E>;
    fn visit_fn_stmt(&mut self, fn_stmt: &FnStmt) -> Result<T, E>;
    fn visit_return_stmt(&mut self, return_stmt: &ReturnStmt) -> Result<T, E>;
}

pub trait ExprVisitor<T, E> {
    fn visit_call_expr(&mut self, expr: &Call) -> Result<T, E>;
    fn visit_binary_expr(&mut self, expr: &BinaryExpr) -> Result<T, E>;
    fn visit_unary_expr(&mut self, expr: &UnaryExpr) -> Result<T, E>;
    fn visit_grouping_expr(&mut self, expr: &Expr) -> Result<T, E>;
    fn visit_var_expr(&mut self, name: &Token) -> Result<T, E>;
    fn visit_assign_expr(&mut self, expr: &AssignExpr) -> Result<T, E>;
    fn visit_list_expr(&mut self, list: &Vec<Expr>) -> Result<T, E>;
    fn visit_member_expr(&mut self, expr: &MemberExpr) -> Result<T, E>;
    fn visit_lit(&mut self, lit: &Lit) -> Result<T, E>;
}

pub fn walk_stmt<T, E>(visitor: &mut dyn StmtVisitor<T, E>, stmt: &Stmt) -> Result<T, E> {
    match stmt {
        Stmt::ExprStmt(expr) => visitor.visit_expr_stmt(expr),
        Stmt::IfStmt(if_stmt) => visitor.visit_if_stmt(if_stmt),
        Stmt::LetStmt(expr) => visitor.visit_let_stmt(expr),
        Stmt::WhileStmt(while_stmt) => visitor.visit_while_stmt(while_stmt),
        Stmt::FnStmt(fn_stmt) => visitor.visit_fn_stmt(fn_stmt),
        Stmt::BlockStmt(stmt_list) => visitor.visit_block_stmt(stmt_list),
        Stmt::ReturnStmt(return_stmt) => visitor.visit_return_stmt(return_stmt)
    }
}

pub fn walk_expr<T, E>(visitor: &mut dyn ExprVisitor<T, E>, expr: &Expr) -> Result<T, E> {
    match expr {
        Expr::Binary(bin_expr) => visitor.visit_binary_expr(bin_expr),
        Expr::Unary(unary_expr) => visitor.visit_unary_expr(unary_expr),
        Expr::Grouping(grouping_expr) => visitor.visit_grouping_expr(grouping_expr),
        Expr::Call(call) => visitor.visit_call_expr(call),
        Expr::Assign(assign_expr) => visitor.visit_assign_expr(assign_expr),
        Expr::Var(var) => visitor.visit_var_expr(var),
        Expr::List(list) => visitor.visit_list_expr(list),
        Expr::Member(member_expr) => visitor.visit_member_expr(member_expr),
        Expr::Lit(lit) => visitor.visit_lit(lit)
    }
}
