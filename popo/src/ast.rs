pub mod visitor;

use std::fmt;
use crate::lexer::tokens::Token;

// TODO Add support for break and continue

#[derive(Debug, PartialEq, Clone)]
pub enum Stmt {
    ExprStmt(Expr),
    IfStmt(IfStmt),
    LetStmt(LetStmt),
    WhileStmt(WhileStmt),
    BlockStmt(Vec<Stmt>),
    FnStmt(FnStmt),
    ReturnStmt(ReturnStmt),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    Binary(Box<BinaryExpr>),
    Unary(Box<UnaryExpr>),
    Grouping(Box<Expr>),
    Call(Box<Call>),
    Assign(Box<AssignExpr>),
    Var(Token),
    List(Vec<Expr>),
    Member(Box<MemberExpr>),
    Lit(Lit),
}

#[derive(Debug, PartialEq, Clone)]
pub struct IfStmt {
    pub token: Token,
    pub condition: Expr,
    pub then_branch: Box<Stmt>,
    pub else_branch: Option<Box<Stmt>>
}

#[derive(Debug, PartialEq, Clone)]
pub struct LetStmt {
    pub var_name: Token,
    pub init: Option<Expr>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct WhileStmt {
    pub token: Token,
    pub condition: Expr,
    pub body: Box<Stmt>
}

#[derive(Debug, PartialEq, Clone)]
pub struct FnStmt {
    pub name: Token,
    pub params: Vec<Token>,
    pub body: Vec<Stmt>
}

#[derive(Debug, PartialEq, Clone)]
pub struct ReturnStmt {
    pub keyword: Token,
    pub value: Option<Expr>
}

#[derive(Debug, PartialEq, Clone)]
pub struct BinaryExpr {
    pub op: Token,
    pub left: Expr,
    pub right: Expr
}

impl BinaryExpr {
    pub fn new(op: Token, left: Expr, right: Expr) -> Self {
        Self { op, left, right }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct UnaryExpr {
    pub op: Token,
    pub right: Expr
}

#[derive(Debug, PartialEq, Clone)]
pub struct AssignExpr {
    pub token: Token,
    pub left: Expr,
    pub right: Expr
}

#[derive(Debug, PartialEq, Clone)]
pub struct Call {
    pub callee: Expr,
    pub args: Vec<Expr>,
    pub paren: Token,
}

#[derive(Debug, PartialEq, Clone)]
pub struct MemberExpr {
    pub object: Expr,
    pub idx: Expr,
    pub bracket: Token
}

#[derive(Debug, PartialEq, Clone)]
pub enum Lit {
    Num(f64),
    Str(String),
    Bool(bool),
    Nil
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expr::Binary(_) => write!(f, "Binary expression"),
            Expr::Unary(_) => write!(f, "Unary expression"),
            Expr::Grouping(_) => write!(f, "Grouping epression"),
            Expr::Call(_) => write!(f, "Call expression"),
            Expr::Assign(_) => write!(f, "Assignment expression"),
            Expr::Var(_) => write!(f, "Variable expression"),
            Expr::List(_) => write!(f, "List expression"),
            Expr::Member(_) => write!(f, "Member expression"),
            Expr::Lit(_) => write!(f, "Literal expression")
        }
    }
}
