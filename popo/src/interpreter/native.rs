use crate::error::PopoError;
use crate::interpreter::{Value, Callable, Interpreter};

use std::io;
use std::fmt;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug)]
pub struct Clock;

impl Callable for Clock {
    fn arity(&self) -> usize {
        0
    }

    fn call(&self, _: &mut Interpreter, _: &[Value], _: usize) -> Result<Value, PopoError> {
        let time = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        Ok(Value::Num(time as f64))
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "clock()")
    }
}

#[derive(Debug)]
pub struct Print;

impl Callable for Print {
    fn arity(&self) -> usize {
        1
    }

    fn call(&self, _: &mut Interpreter, args: &[Value], _: usize) -> Result<Value, PopoError> {
        println!("{}", args[0]);
        Ok(Value::Nil)
    }
}

impl fmt::Display for Print {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "print()")
    }
}

pub struct Len;

impl Callable for Len {
    fn arity(&self) -> usize {
        1
    }

    fn call(&self, _: &mut Interpreter, args: &[Value], call_line: usize) -> Result<Value, PopoError> {
        match &args[0] {
            Value::Indexable(indexable) => Ok(indexable.borrow().len()),
            _ => Err(PopoError::new(
                call_line,
                format!("{} has no length", args[0])
            ))
        }
    }
}

impl fmt::Display for Len {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "len()")
    }
}

pub struct Scan;

impl Callable for Scan {
    fn arity(&self) -> usize {
        0
    }

    fn call(&self, _: &mut Interpreter, _: &[Value], call_line: usize) -> Result<Value, PopoError> {
        let mut buffer = String::new();
        let stdin = io::stdin();
        match stdin.read_line(&mut buffer) {
            Ok(_) => Ok(Value::Str(buffer.trim().to_string())),
            Err(err) => Err(PopoError::new(call_line, err.to_string()))
        }
    }
}

impl fmt::Display for Scan {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "scanln()")
    }
}
