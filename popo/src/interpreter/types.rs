use crate::error::PopoError;
use crate::ast::FnStmt;
use super::{Interpreter, ReturnValue, environment::Environment};

use std::rc::Rc;
use std::cell::RefCell;

use std::fmt;

#[derive(Clone)]
pub enum Value {
    Callable(Rc<dyn Callable>),
    Indexable(Rc<RefCell<dyn Indexable>>),
    Num(f64),
    Str(String),
    Bool(bool),
    Nil
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Callable(callable) => write!(f, "{}", callable),
            Value::Indexable(indexable) => write!(f, "{}", indexable.borrow()),
            Value::Num(num) => write!(f, "{}", num.to_string()),
            Value::Str(text) => write!(f, "{}", text),
            Value::Bool(val) => write!(f, "{}", val),
            Value::Nil => write!(f, "nil")
        }
    }
}

pub trait Indexable: fmt::Display {
    fn get(&self, idx: Value) -> Result<Value, String>;
    fn set(&mut self, idx: Value, val: Value) -> Result<(), String>;
    fn len(&self) -> Value;
}

pub struct PopoList {
    data: Vec<Value>
}

impl PopoList {
    pub fn new(data: Vec<Value>) -> Self {
        Self { data }
    }

    fn check_idx(&self, idx: Value) -> Result<usize, String> {
        match idx {
            Value::Num(idx_val) => {
                let idx_val_usize = idx_val as usize;
                match self.data.len() <= idx_val_usize {
                    true => Err("Index out of bounds".to_string()),
                    false => Ok(idx_val_usize)
                }
            }
            _ => Err(format!("{} is not a valid list index", idx))
        }
    }
}

impl Indexable for PopoList {
    fn get(&self, idx: Value) -> Result<Value, String> {
        let idx_val = self.check_idx(idx)?;
        Ok(self.data[idx_val].clone())
    }

    fn set(&mut self, idx: Value, val: Value) -> Result<(), String> {
        let idx_val = self.check_idx(idx)?;
        self.data[idx_val] = val;
        Ok(())
    }

    fn len(&self) -> Value {
        Value::Num(self.data.len() as f64)
    }
}

impl fmt::Display for PopoList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut out = String::from("[");
        let mut idx = 0;
        while idx < self.data.len() {
            out.push_str(&self.data[idx].to_string());
            if idx != self.data.len() - 1 {
                out.push_str(",");
            }
            idx += 1;
        }
        out.push_str("]");
        write!(f, "{}", out)
    }
}

pub trait Callable: fmt::Display {
    fn arity(&self) -> usize;
    fn call(&self, interpreter: &mut Interpreter, args: &[Value], call_line: usize) -> Result<Value, PopoError>;
}

pub struct PopoFunction {
    decl: FnStmt,
    closure: Rc<RefCell<Environment>>
}

impl PopoFunction {
    pub fn new(decl: FnStmt, closure: Rc<RefCell<Environment>>) -> Self {
        Self { decl, closure }
    }
}

impl Callable for PopoFunction {
    fn arity(&self) -> usize {
        self.decl.params.len()
    }

    fn call(&self, interpreter: &mut Interpreter, args: &[Value], _: usize) -> Result<Value, PopoError> {
        let mut env = Environment::new_with_parent(self.closure.clone());
        for i in 0..args.len() {
            env.define(&self.decl.params[i].value, args[i].clone());
        }
        match interpreter.execute_block(&self.decl.body, env) {
            Ok(()) => Ok(Value::Nil),
            Err(err) => match err {
                ReturnValue::Return { keyword: _, value } => Ok(value),
                ReturnValue::Err(err) => Err(err)
            }
        }
    }
}

impl fmt::Display for PopoFunction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<fn {}>", self.decl.name.value)
    }
}
