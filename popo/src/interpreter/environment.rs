use crate::lexer::tokens::Token;
use crate::error::PopoError;
use super::{Value, native::*};

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

#[derive(Default)]
pub struct Environment {
    lookup_table: HashMap<String, Value>,
    parent: Option<Rc<RefCell<Environment>>>
}

impl Environment {
    pub fn global() -> Self {
        let mut global_env = Environment::default();
        global_env.define("clock", Value::Callable(Rc::new(Clock{})));
        global_env.define("print", Value::Callable(Rc::new(Print{})));
        global_env.define("len", Value::Callable(Rc::new(Len{})));
        global_env.define("scan", Value::Callable(Rc::new(Scan{})));
        global_env
    }

    pub fn new_with_parent(env: Rc<RefCell<Environment>>) -> Self {
        Self {
            lookup_table: HashMap::new(),
            parent: Some(env)
        }
    }

    pub fn define(&mut self, name: &str, val: Value) {
        self.lookup_table.insert(
            name.to_string(),
            val
        );
    }

    pub fn assign(&mut self, name: &Token, val: Value) -> Result<(), PopoError> {
        if !self.lookup_table.contains_key(&name.value) {
            match &self.parent {
                Some(env) => return env.borrow_mut().assign(name, val),
                None => return Err(get_undefined_var_err(name))
            }
        }

        self.lookup_table.insert(name.value.clone(), val);
        Ok(())
    }

    pub fn assign_at(&mut self, name: &Token, val: Value, distance: usize) -> Result<(), PopoError> {
        if distance == 0 {
            return self.assign(name, val);
        }
        self.ancestor(distance).borrow_mut().assign(name, val)
    }

    pub fn get(&self, name: &Token) -> Result<Value, PopoError> {
        match self.lookup_table.get(&name.value) {
            Some(val) => Ok(val.clone()),
            None => match &self.parent {
                Some(env) => env.borrow().get(name),
                None => Err(get_undefined_var_err(name))
            }
        }
    }

    pub fn get_at(&self, name: &Token, distance: usize) -> Result<Value, PopoError> {
        if distance == 0 {
            return self.get(name);
        }
        self.ancestor(distance).borrow().get(name)
    }

    fn ancestor(&self, distance: usize) -> Rc<RefCell<Environment>> {
        let assert_msg = "Resolver incorrectly calculates var distance";
        let mut env = self.parent.clone().expect(assert_msg);
        for _ in 1..distance {
            let new_env = env.borrow().parent.clone().expect(assert_msg);
            env = new_env;
        }
        env
    }
}

fn get_undefined_var_err(name: &Token) -> PopoError {
    PopoError {
        line: name.line,
        msg: format!("Undefined variable {}", name.value)
    }
}
