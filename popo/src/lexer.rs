pub mod tokens;

use lazy_static::lazy_static;
use regex::Regex;
use crate::error::PopoError;
use crate::lexer::tokens::{TokenKind, Token};

pub struct Tokenizer {
    content: String,
    cursor: usize,
    line: usize,
    curr_id: usize,
}

impl Tokenizer {
    pub fn new(content: String) -> Self {
        Self {
            content,
            cursor: 0,
            line: 1,
            curr_id: 0
        }
    }

    pub fn get_all_tokens(&mut self) -> (Vec<Token>, Vec<PopoError>) {
        let mut tokens = Vec::new();
        let mut errors = Vec::new();

        loop {
            match self.get_next_token() {
                Ok(token) => {
                    tokens.push(token.clone());
                    if token.kind == TokenKind::Eof {
                        break;
                    }
                }
                Err(error) => errors.push(error)
            }
        }

        (tokens, errors)
    }

    fn get_next_token(&mut self) -> Result<Token, PopoError> {
        if !self.has_tokens() {
            return Ok(Token::new(
                self.next_id(),
                TokenKind::Eof,
                String::from("EOF"),
                self.line
            ))
        }

        let sliced_content = self.content[self.cursor..self.content.len()].to_string();
        for rule in RULES.iter() {
            let token_kind = rule.0;
            let pattern = &rule.1;

            if let Some(matched) = pattern.find(&sliced_content) {
                self.cursor += matched.start() + matched.end();

                if token_kind == TokenKind::Empty {
                    return self.get_next_token();
                }

                if token_kind == TokenKind::Newline {
                    self.line += 1;
                    return self.get_next_token();
                }

                let token_val = &sliced_content[matched.start()..matched.end()];

                if token_kind == TokenKind::Identifier {
                    if let Some(token) = self.lookup_identifier(&token_val) {
                        return Ok(token);
                    }
                }

                return Ok(Token::new(
                    self.next_id(),
                    token_kind,
                    String::from(token_val),
                    self.line,
                ));
            }
        }

        self.cursor += 1;
        Err(PopoError::new(
            self.line,
            format!("Unknown token {}", &sliced_content.chars().nth(0).unwrap()),
        ))
    }

    fn lookup_identifier(&mut self, identifier: &str) -> Option<Token> {
        for keyword in KEYWORDS.iter() {
            let token_kind = keyword.0;
            let pattern = &keyword.1;

            if let Some(_) = pattern.find(identifier) {
                return Some(Token::new(
                    self.next_id(),
                    token_kind,
                    String::from(identifier),
                    self.line,
                ));
            }
        }

        None
    }

    fn has_tokens(&self) -> bool {
        self.cursor < self.content.len()
    }

    fn next_id(&mut self) -> usize {
        let id = self.curr_id;
        self.curr_id = self.curr_id + 1;
        id
    }
}

lazy_static! {
    static ref RULES: Vec<(TokenKind, Regex)> = vec![

        /* Comments and whitespaces */
        (TokenKind::Newline, Regex::new(r"^\n").unwrap()),
        (TokenKind::Empty, Regex::new(r"^\s+").unwrap()),
        (TokenKind::Empty, Regex::new(r"^#.*").unwrap()),

        /* Literals */
        (TokenKind::Number, Regex::new(r"^(\d+(?:\.\d+)?)").unwrap()),
        (TokenKind::Str, Regex::new(r#"^"[^"]*""#).unwrap()),
        (TokenKind::Identifier, Regex::new(r"^[a-zA-Z_][a-zA-Z_0-9]*").unwrap()),

        /* Blocks */
        (TokenKind::LCurlyBrace, Regex::new(r"^\{").unwrap()),
        (TokenKind::RCurlyBrace, Regex::new(r"^\}").unwrap()),

        /* Expressions */
        (TokenKind::Semicolon, Regex::new(r"^;").unwrap()),
        (TokenKind::Plus, Regex::new(r"^[+]").unwrap()),
        (TokenKind::Minus, Regex::new(r"^[-]").unwrap()),
        (TokenKind::Star, Regex::new(r"^[*]").unwrap()),
        (TokenKind::Slash, Regex::new(r"^[/]").unwrap()),
        (TokenKind::LParenthesis, Regex::new(r"^\(").unwrap()),
        (TokenKind::RParenthesis, Regex::new(r"^\)").unwrap()),
        (TokenKind::LSqrBracket, Regex::new(r"^\[").unwrap()),
        (TokenKind::RSqrBracket, Regex::new(r"^\]").unwrap()),

        /* Assignment and comparison */
        (TokenKind::BangEq, Regex::new(r"^!=").unwrap()),
        (TokenKind::Bang, Regex::new(r"^!").unwrap()),
        (TokenKind::Eq, Regex::new(r"^==").unwrap()),
        (TokenKind::Assign, Regex::new(r"^=").unwrap()),
        (TokenKind::GreaterEq, Regex::new(r"^>=").unwrap()),
        (TokenKind::Greater, Regex::new(r"^>").unwrap()),
        (TokenKind::LessEq, Regex::new(r"^<=").unwrap()),
        (TokenKind::Less, Regex::new(r"^<").unwrap()),

        /* Single char tokens */
        (TokenKind::Comma, Regex::new(r"^,").unwrap()),
    ];

    static ref KEYWORDS: Vec<(TokenKind, Regex)> = vec![
        (TokenKind::True, Regex::new(r"^true$").unwrap()),
        (TokenKind::False, Regex::new(r"^false$").unwrap()),
        (TokenKind::Nil, Regex::new(r"^nil$").unwrap()),
        (TokenKind::If, Regex::new(r"^if$").unwrap()),
        (TokenKind::Else, Regex::new(r"^else$").unwrap()),
        (TokenKind::And, Regex::new(r"^and$").unwrap()),
        (TokenKind::Or, Regex::new(r"^or$").unwrap()),
        (TokenKind::For, Regex::new(r"^for$").unwrap()),
        (TokenKind::While, Regex::new(r"^while$").unwrap()),
        (TokenKind::Let, Regex::new(r"^let$").unwrap()),
        (TokenKind::Fn, Regex::new(r"^fn$").unwrap()),
        (TokenKind::Return, Regex::new(r"^return$").unwrap()),
    ];
}
