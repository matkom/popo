use crate::error::PopoError;
use crate::lexer::tokens::{Token, TokenKind};
use crate::ast::*;

pub struct Parser {
    tokens: Vec<Token>,
    cursor: usize,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self {
            tokens,
            cursor: 0
        }
    }

    pub fn parse(&mut self) -> Result<Vec<Stmt>, Vec<PopoError>> {
        let mut stmts = Vec::new();
        let mut errs = Vec::new();

        while !self.is_eof() && self.peek().kind != TokenKind::Eof {
            match self.declaration() {
                Ok(stmt) => stmts.push(stmt),
                Err(err) => {
                    errs.push(err);
                    self.synchronize();
                }
            };
        }

        match errs.is_empty() {
            true => Ok(stmts),
            false => Err(errs)
        }
    }

    fn declaration(&mut self) -> Result<Stmt, PopoError> {
        match self.peek().kind {
            TokenKind::Let => self.let_declaration(),
            TokenKind::Fn => self.fn_declaration(),
            _ => self.stmt()
        }
    }

    fn let_declaration(&mut self) -> Result<Stmt, PopoError> {
        self.eat(TokenKind::Let)?;
        let var_name = self.eat(TokenKind::Identifier)?.clone();
        let mut init = None;

        if self.peek().kind == TokenKind::Assign {
            self.eat(TokenKind::Assign)?;
            init = Some(self.expr()?);
        }

        self.eat(TokenKind::Semicolon)?;
        Ok(Stmt::LetStmt(LetStmt{var_name, init}))
    }

    fn fn_declaration(&mut self) -> Result<Stmt, PopoError> {
        self.eat(TokenKind::Fn)?;
        let name = self.eat(TokenKind::Identifier)?.clone();

        self.eat(TokenKind::LParenthesis)?;
        let mut params = Vec::new();
        if self.peek().kind != TokenKind::RParenthesis {
            loop {
                if params.len() >= 255 {
                    return Err(PopoError::new(
                        self.peek().line,
                        "Function cannot have more than 255 parameters".to_string()
                    ));
                }

                let param = self.eat(TokenKind::Identifier)?.clone();
                params.push(param);

                if self.peek().kind != TokenKind::Comma {
                    break;
                }
                self.eat(TokenKind::Comma)?;
            }
        }
        self.eat(TokenKind::RParenthesis)?;

        let body: Vec<Stmt>;
        if let Stmt::BlockStmt(stmts) = self.block_stmt()? {
            body = stmts
        } else {
            unreachable!("Block stmt function did not return block stmt!")
        }

        Ok(Stmt::FnStmt(FnStmt{
            name,
            params,
            body
        }))
    }

    fn stmt(&mut self) -> Result<Stmt, PopoError> {
        match self.peek().kind {
            TokenKind::If => self.if_stmt(),
            TokenKind::While => self.while_stmt(),
            TokenKind::For => self.for_stmt(),
            TokenKind::LCurlyBrace => self.block_stmt(),
            TokenKind::Return => self.return_stmt(),
            _ => self.expr_stmt(),
        }
    }

    fn if_stmt(&mut self) -> Result<Stmt, PopoError> {
        let token = self.eat(TokenKind::If)?.clone();
        let condition = self.expr()?;
        let then_branch = Box::new(self.block_stmt()?);
        let mut else_branch = None;

        if self.peek().kind == TokenKind::Else {
            self.eat(TokenKind::Else)?;
            else_branch = Some(Box::new(self.block_stmt()?));
        }

        Ok(Stmt::IfStmt(IfStmt {
            token,
            condition,
            then_branch,
            else_branch
        }))
    }

    fn while_stmt(&mut self) -> Result<Stmt, PopoError> {
        let token = self.eat(TokenKind::While)?.clone();
        let condition = self.expr()?;
        let body = self.block_stmt()?;
        Ok(Stmt::WhileStmt(WhileStmt{
            token,
            condition,
            body: Box::new(body)
        }))
    }

    fn for_stmt(&mut self) -> Result<Stmt, PopoError> {
        let for_token = self.eat(TokenKind::For)?.clone();
        self.eat(TokenKind::LParenthesis)?;

        let initializer: Option<Stmt>;
        let mut condition: Option<Expr> = None;
        let mut increment: Option<Expr> = None;
        let mut body: Stmt;

        match self.peek().kind {
            TokenKind::Semicolon => {
                self.eat(TokenKind::Semicolon)?;
                initializer = None;
            }
            TokenKind::Let => {
                initializer = Some(self.let_declaration()?);
            }
            _ => {
                initializer = Some(self.expr_stmt()?);
            }
        }

        if self.peek().kind != TokenKind::Semicolon {
            condition = Some(self.expr()?);
        }
        self.eat(TokenKind::Semicolon)?;

        if self.peek().kind != TokenKind::RParenthesis {
            increment = Some(self.expr()?);
        }
        self.eat(TokenKind::RParenthesis)?;

        body = self.block_stmt()?;
        if let Some(val) = increment {
            body = Stmt::BlockStmt(vec![body, Stmt::ExprStmt(val)]);
        }

        if condition.is_none() {
            condition = Some(Expr::Lit(Lit::Bool(true)));
        }

        body = Stmt::WhileStmt(WhileStmt {
            token: for_token,
            condition: condition.unwrap(),
            body: Box::new(body)
        });

        if let Some(val) = initializer {
            body = Stmt::BlockStmt(vec![val, body]);
        }

        Ok(body)
    }

    fn block_stmt(&mut self) -> Result<Stmt, PopoError> {
        self.eat(TokenKind::LCurlyBrace)?;
        let mut stmt_list: Vec<Stmt> = Vec::new();

        while !self.is_eof() && self.peek().kind != TokenKind::RCurlyBrace {
            stmt_list.push(self.declaration()?);
        }

        self.eat(TokenKind::RCurlyBrace)?;
        Ok(Stmt::BlockStmt(stmt_list))
    }

    fn return_stmt(&mut self) -> Result<Stmt, PopoError> {
        let keyword = self.eat(TokenKind::Return)?.clone();
        let mut value: Option<Expr> = None;

        if self.peek().kind != TokenKind::Semicolon {
            value = Some(self.expr()?);
        }

        self.eat(TokenKind::Semicolon)?;
        Ok(Stmt::ReturnStmt(ReturnStmt{keyword, value}))
    }

    fn expr_stmt(&mut self) -> Result<Stmt, PopoError> {
        let expr = self.expr()?;
        self.eat(TokenKind::Semicolon)?;
        Ok(Stmt::ExprStmt(expr))
    }

    fn expr(&mut self) -> Result<Expr, PopoError> {
        self.assign_expr()
    }

    fn assign_expr(&mut self) -> Result<Expr, PopoError> {
        let left = self.or_expr()?;

        if self.peek().kind != TokenKind::Assign {
            return Ok(left);
        }

        let token = self.eat(TokenKind::Assign)?.clone();
        let right = self.assign_expr()?;

        Ok(Expr::Assign(Box::new(AssignExpr { token, left, right })))
    }

    fn or_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::and_expr, vec![TokenKind::Or])
    }

    fn and_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::eq_expr, vec![TokenKind::And])
    }

    fn eq_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::comparison_expr, vec![TokenKind::BangEq, TokenKind::Eq])
    }

    fn comparison_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::term_expr, vec![TokenKind::Greater, TokenKind::GreaterEq, TokenKind::Less, TokenKind::LessEq])
    }

    fn term_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::factor_expr, vec![TokenKind::Plus, TokenKind::Minus])
    }

    fn factor_expr(&mut self) -> Result<Expr, PopoError> {
        self.binary_expr(&Parser::unary_expr, vec![TokenKind::Star, TokenKind::Slash])
    }

    fn binary_expr(&mut self, builder: &dyn Fn(&mut Parser) -> Result<Expr, PopoError>, ops: Vec<TokenKind>) -> Result<Expr, PopoError> {
        let mut left = builder(self)?;

        while self.eat_any(&ops) {
            let op = self.previous().clone();
            let right = builder(self)?;
            left = Expr::Binary(Box::new(
                BinaryExpr{op, left, right}
            ));
        }

        Ok(left)
    }

    fn unary_expr(&mut self) -> Result<Expr, PopoError> {
        if self.eat_any(&vec![TokenKind::Bang, TokenKind::Minus]) {
            let op = self.previous().clone();
            let right = self.unary_expr()?;
            return Ok(Expr::Unary(Box::new(
                UnaryExpr{op, right}
            )))
        }

        let expr = self.primary_expr()?;
        match self.peek().kind {
            TokenKind::LParenthesis => self.call_expr(expr),
            TokenKind::LSqrBracket => self.member_expr(expr),
            _ => Ok(expr)
        }
    }

    fn call_expr(&mut self, callee: Expr) -> Result<Expr, PopoError> {
        let mut call_expr = callee;

        while self.peek().kind == TokenKind::LParenthesis {
            self.eat(TokenKind::LParenthesis)?;

            let mut args = Vec::new();
            if self.peek().kind != TokenKind::RParenthesis {
                loop {
                    if args.len() >= 255 {
                        return Err(PopoError::new(
                            self.peek().line,
                            "Call cannot have more than 255 arguments".to_string()
                        ))
                    }

                    args.push(self.expr()?);
                    match self.peek().kind {
                        TokenKind::Comma => { self.eat(TokenKind::Comma)?; }
                        _ => break
                    }
                }
            }

            let paren = self.eat(TokenKind::RParenthesis)?.clone();
            call_expr = Expr::Call(Box::new(Call{callee: call_expr, args, paren}));
        }

        Ok(call_expr)
    }

    fn member_expr(&mut self, object: Expr) -> Result<Expr, PopoError> {
        let mut member_expr = object;
        while self.peek().kind == TokenKind::LSqrBracket {
            self.eat(TokenKind::LSqrBracket)?;
            let idx = self.expr()?;
            let bracket = self.eat(TokenKind::RSqrBracket)?.clone();
            member_expr = Expr::Member(Box::new(MemberExpr {
                object: member_expr,
                idx,
                bracket
            }));
        }
        Ok(member_expr)
    }

    fn primary_expr(&mut self) -> Result<Expr, PopoError> {
        match self.peek().kind {
            TokenKind::LParenthesis => self.grouping_expr(),
            TokenKind::Identifier => self.var_expr(),
            TokenKind::LSqrBracket => self.list_expr(),
            _ => Ok(Expr::Lit(self.literal()?)),
        }
    }

    fn grouping_expr(&mut self) -> Result<Expr, PopoError> {
        self.eat(TokenKind::LParenthesis)?;
        let expr = self.expr()?;
        self.eat(TokenKind::RParenthesis)?;
        Ok(Expr::Grouping(Box::new(expr)))
    }

    fn var_expr(&mut self) -> Result<Expr, PopoError> {
        let name = self.eat(TokenKind::Identifier)?;
        Ok(Expr::Var(name.clone()))
    }

    fn list_expr(&mut self) -> Result<Expr, PopoError> {
        self.eat(TokenKind::LSqrBracket)?;
        let mut args = Vec::new();
        if self.peek().kind != TokenKind::RSqrBracket {
            loop {
                args.push(self.expr()?);
                if self.peek().kind != TokenKind::Comma {
                    break;
                }
                self.eat(TokenKind::Comma)?;
            }
        }
        self.eat(TokenKind::RSqrBracket)?;
        Ok(Expr::List(args))
    }

    fn literal(&mut self) -> Result<Lit, PopoError> {
        let token = self.peek();
        match token.kind {
            TokenKind::Number => {
                let token = self.eat(TokenKind::Number)?;
                Ok(Lit::Num(token.value.parse().unwrap()))
            }
            TokenKind::Str => {
                let token = self.eat(TokenKind::Str)?;
                Ok(Lit::Str((&token.value[1..token.value.len() - 1]).to_string()))
            }
            TokenKind::True => {
                self.eat(TokenKind::True)?;
                Ok(Lit::Bool(true))
            }
            TokenKind::False => {
                self.eat(TokenKind::False)?;
                Ok(Lit::Bool(false))
            }
            TokenKind::Nil => {
                self.eat(TokenKind::Nil)?;
                Ok(Lit::Nil)
            }
            _ => Err(PopoError{
                line: token.line,
                msg: format!("Unknown literal: {}", token.value)
            })
        }
    }

    fn synchronize(&mut self) {
        self.advance();

        while !self.is_eof() {
            if self.previous().kind == TokenKind::Semicolon {
                break;
            }

            match self.peek().kind {
                TokenKind::Fn
                | TokenKind::Let
                | TokenKind::For
                | TokenKind::While
                | TokenKind::If
                | TokenKind::Return => break,
                _ => {
                    self.advance();
                }
            }
        }
    }

    fn eat(&mut self, token_kind: TokenKind) -> Result<&Token, PopoError> {
        let token = self.peek();

        if token.kind == TokenKind::Eof {
            return Err(PopoError{
                line: token.line,
                msg: format!("Unexpected end of input, expected: {}", token_kind)
            })
        }

        if token.kind != token_kind {
            return Err(PopoError{
                line: token.line,
                msg: format!("Unexpected token: {}, expected: {}", token.value, token_kind)
            })
        }

        Ok(self.advance())
    }

    fn eat_any(&mut self, token_kinds: &Vec<TokenKind>) -> bool {
        for kind in token_kinds {
            if self.is_eatable(*kind) {
                self.advance();
                return true
            }
        }
        false
    }

    fn is_eatable(&self, token_kind: TokenKind) -> bool {
        if self.is_eof() {
            return false;
        }
        self.peek().kind == token_kind
    }

    fn advance(&mut self) -> &Token {
        if !self.is_eof() {
            self.cursor += 1;
        }
        self.previous()
    }

    fn peek(&self) -> &Token {
        self.get_token(self.cursor)
    }

    fn previous(&self) -> &Token {
        self.get_token(self.cursor - 1)
    }

    fn get_token(&self, index: usize) -> &Token {
        &self.tokens.get(index).expect(
            "Fatal parser error. Tried accessing non existent index inside tokens array."
        )
    }

    fn is_eof(&self) -> bool {
        self.peek().kind == TokenKind::Eof
    }
}
