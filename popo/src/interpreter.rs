pub mod environment;
pub mod native;
pub mod types;

use crate::error::*;
use crate::ast::*;
use crate::ast::visitor::*;
use crate::lexer::tokens::{Token, TokenKind};
use crate::interpreter::environment::Environment;
use crate::interpreter::types::{Value, Callable, PopoFunction, Indexable, PopoList};

use std::mem;
use std::rc::Rc;
use std::cell::RefCell;
use std::convert::From;
use std::collections::HashMap;

pub struct Interpreter {
    curr_env: Rc<RefCell<Environment>>,
    globals: Rc<RefCell<Environment>>,
    locals: HashMap<usize, usize>
}

impl Interpreter {
    pub fn new() -> Self {
        let global_env = Rc::new(RefCell::new(Environment::global()));
        Self {
            curr_env: global_env.clone(),
            globals: global_env,
            locals: HashMap::new()
        }
    }

    pub fn new_with_env(env: Rc<RefCell<Environment>>) -> Self {
        Self {
            curr_env: env.clone(),
            globals: env,
            locals: HashMap::new()
        }
    }

    pub fn interpret(&mut self, ast: &Vec<Stmt>) {
        for stmt in ast {
            if let Err(err) = walk_stmt(self, stmt) {
                match err {
                    ReturnValue::Err(err) => report_error(err),
                    ReturnValue::Return { keyword: _, value: _} => {
                      unreachable!("Resolver did not handle wrong return statement");
                    }
                }
                break;
            }
        }
    }

    pub fn execute_block(&mut self, stmts: &Vec<Stmt>, env: Environment) -> Result<(), ReturnValue> {
        let mut block_env = Rc::new(RefCell::new(env));
        mem::swap(&mut self.curr_env, &mut block_env);
        for stmt in stmts {
            match walk_stmt(self, stmt) {
                Ok(()) => {}
                Err(err) => {
                    mem::swap(&mut self.curr_env, &mut block_env);
                    return Err(err);
                }
            }
        }
        mem::swap(&mut self.curr_env, &mut block_env);
        Ok(())
    }

    pub fn resolve(&mut self, name: &Token, depth: usize) {
        self.locals.insert(name.id, depth);
    }

    fn lookup_var(&self, name: &Token) -> Result<Value, PopoError> {
        match self.locals.get(&name.id) {
            Some(distance) => self.curr_env.borrow().get_at(name, *distance),
            None => self.globals.borrow().get(name)
        }
    }
}

impl StmtVisitor<(), ReturnValue> for Interpreter {
    fn visit_expr_stmt(&mut self, expr: &Expr) -> Result<(), ReturnValue> {
        walk_expr(self, expr)?;
        Ok(())
    }

    fn visit_if_stmt(&mut self, if_stmt: &IfStmt) -> Result<(), ReturnValue> {
        let val = walk_expr(self, &if_stmt.condition)?;

        if check_bool(&val, &if_stmt.token)? {
            return walk_stmt(self, &if_stmt.then_branch)
        } else if let Some(else_branch) = &if_stmt.else_branch {
            return walk_stmt(self, &else_branch)
        }

        Ok(())
    }

    fn visit_while_stmt(&mut self, while_stmt: &WhileStmt) -> Result<(), ReturnValue> {
        while check_bool(&walk_expr(self, &while_stmt.condition)?, &while_stmt.token)? {
            walk_stmt(self, &while_stmt.body)?;
        }
        Ok(())
    }

    fn visit_let_stmt(&mut self, let_stmt: &LetStmt) -> Result<(), ReturnValue> {
        let var_value: Value;
        match &let_stmt.init {
            Some(init) => var_value = walk_expr(self, &init)?,
            None => var_value = Value::Nil
        }
        let var_name = let_stmt.var_name.value.clone();
        self.curr_env.borrow_mut().define(&var_name, var_value);
        Ok(())
    }

    fn visit_block_stmt(&mut self, stmts: &Vec<Stmt>) -> Result<(), ReturnValue> {
        self.execute_block(stmts, Environment::new_with_parent(self.curr_env.clone()))
    }

    fn visit_fn_stmt(&mut self, fn_stmt: &FnStmt) -> Result<(), ReturnValue> {
        let function = Value::Callable(Rc::new(PopoFunction::new(fn_stmt.clone(), self.curr_env.clone())));
        let name = &fn_stmt.name.value;
        self.curr_env.borrow_mut().define(name, function);
        Ok(())
    }

    fn visit_return_stmt(&mut self, return_stmt: &ReturnStmt) -> Result<(), ReturnValue> {
        let mut return_val = Value::Nil;
        if let Some(value) = &return_stmt.value {
            return_val = walk_expr(self, value)?;
        }
        Err(ReturnValue::Return {
            keyword: return_stmt.keyword.clone(),
            value: return_val
        })
    }
}

impl ExprVisitor<Value, PopoError> for Interpreter {
    fn visit_call_expr(&mut self, call_expr: &Call) -> Result<Value, PopoError> {
        let callee = walk_expr(self, &call_expr.callee)?;
        let mut args = Vec::new();

        for arg in &call_expr.args {
            args.push(walk_expr(self, arg)?)
        }

        if let Value::Callable(callable) = callee {
            let args_len = call_expr.args.len();
            let callable_arity = callable.arity();

            if args_len != callable_arity {
                return Err(PopoError::new(
                    call_expr.paren.line,
                    format!("{} expected {} arguments but got {}", callable, callable_arity, args_len)
                ));
            }

            callable.call(self, &args, call_expr.paren.line)
        } else {
            Err(PopoError::new(
                call_expr.paren.line,
                "Can only call functions and classes".to_string()
            ))
        }
    }

    fn visit_binary_expr(&mut self, expr: &BinaryExpr) -> Result<Value, PopoError> {
        let left = walk_expr(self, &expr.left)?;
        let op_kind = expr.op.kind;

        if op_kind == TokenKind::Or {
            if check_bool(&left, &expr.op)? {
                return Ok(left);
            }
            return walk_expr(self, &expr.right);
        } else if op_kind == TokenKind::And {
            if !check_bool(&left, &expr.op)? {
                return Ok(left);
            }
            return walk_expr(self, &expr.right);
        }

        let right = walk_expr(self, &expr.right)?;
        match (&left, op_kind, &right) {
            (Value::Num(lhs), op, Value::Num(rhs)) => match op {
                TokenKind::Plus => Ok(Value::Num(lhs + rhs)),
                TokenKind::Minus => Ok(Value::Num(lhs - rhs)),
                TokenKind::Star => Ok(Value::Num(lhs * rhs)),
                TokenKind::Slash => match *rhs != 0.0 {
                    true => Ok(Value::Num(lhs / rhs)),
                    false => Err(PopoError::new(
                        expr.op.line,
                        "Division by 0".to_string()
                    ))
                }
                TokenKind::Greater => Ok(Value::Bool(lhs > rhs)),
                TokenKind::GreaterEq => Ok(Value::Bool(lhs >= rhs)),
                TokenKind::Less => Ok(Value::Bool(lhs < rhs)),
                TokenKind::LessEq => Ok(Value::Bool(lhs <= rhs)),
                TokenKind::Eq => Ok(Value::Bool(lhs == rhs)),
                TokenKind::BangEq => Ok(Value::Bool(lhs != rhs)),
                _ => Err(PopoError::new(
                    expr.op.line,
                    format!("Cannot perform {} on numbers", op_kind)
                ))
            }
            (Value::Str(lhs), op, Value::Str(rhs)) => match op {
                TokenKind::Eq => Ok(Value::Bool(lhs == rhs)),
                TokenKind::BangEq => Ok(Value::Bool(lhs != rhs)),
                TokenKind::Plus => Ok(Value::Str(format!("{}{}", lhs, rhs))),
                _ => Err(PopoError::new(
                    expr.op.line,
                    format!("Cannot perform {} on strings", op_kind)
                ))
            }
            (Value::Bool(lhs), op, Value::Bool(rhs)) => match op {
                TokenKind::Eq => Ok(Value::Bool(lhs == rhs)),
                TokenKind::BangEq => Ok(Value::Bool(lhs != rhs)),
                _ => Err(PopoError::new(
                    expr.op.line,
                    format!("Cannot perform {} on booleans", op_kind)
                ))
            }
            (Value::Str(lhs), TokenKind::Plus, rhs) => {
                Ok(Value::Str(format!("{}{}", lhs, rhs)))
            }
            (lhs, TokenKind::Plus, Value::Str(rhs)) => {
                Ok(Value::Str(format!("{}{}", lhs, rhs)))
            }
            _ => Err(PopoError::new(
                expr.op.line,
                format!("Cannot perform {} on {} and {}", op_kind, left, right)
            ))
        }
    }

    fn visit_unary_expr(&mut self, expr: &UnaryExpr) -> Result<Value, PopoError> {
        let right = walk_expr(self, &expr.right)?;
        let op_kind = expr.op.kind;
        let token = &expr.op;

        match (op_kind, &right) {
            (TokenKind::Minus, Value::Num(rhs)) => Ok(Value::Num(-rhs)),
            (TokenKind::Bang, Value::Bool(rhs)) => Ok(Value::Bool(!rhs)),
            _ => Err(PopoError::new(
                token.line,
                format!("Unknown unary operation: {} for {}", op_kind, right)
            ))
        }
    }

    fn visit_grouping_expr(&mut self, expr: &Expr) -> Result<Value, PopoError> {
        walk_expr(self, expr)
    }

    fn visit_var_expr(&mut self, name: &Token) -> Result<Value, PopoError> {
        self.lookup_var(name)
    }

    fn visit_assign_expr(&mut self, expr: &AssignExpr) -> Result<Value, PopoError> {
        match &expr.left {
            Expr::Var(var_name) => {
                let value = walk_expr(self, &expr.right)?;
                match self.locals.get(&var_name.id) {
                    Some(distance) => self.curr_env.borrow_mut().assign_at(&var_name, value.clone(), *distance)?,
                    None => self.globals.borrow_mut().assign(&var_name, value.clone())?
                }
                Ok(value)
            }
            Expr::Member(member_expr) => {
                let value = walk_expr(self, &expr.right)?;
                let object = walk_expr(self, &member_expr.object)?;
                let indexable = check_indexable(&object, &member_expr.bracket)?;
                let idx = walk_expr(self, &member_expr.idx)?;
                let result = match indexable.borrow_mut().set(idx, value.clone()) {
                    Ok(()) => Ok(value),
                    Err(err_msg) => Err(PopoError::new(member_expr.bracket.line, err_msg))
                };
                result
            }
            _ => Err(PopoError::new(
                expr.token.line,
                format!("Unknown assignment target: {}", expr.left)
            ))
        }
    }

    fn visit_list_expr(&mut self, list: &Vec<Expr>) -> Result<Value, PopoError> {
        let mut data = Vec::new();
        for item in list {
            let val = walk_expr(self, item)?;
            data.push(val);
        }
        Ok(Value::Indexable(Rc::new(RefCell::new(PopoList::new(data)))))
    }

    fn visit_member_expr(&mut self, expr: &MemberExpr) -> Result<Value, PopoError> {
        let object = walk_expr(self, &expr.object)?;
        let indexable = check_indexable(&object, &expr.bracket)?;
        let idx = walk_expr(self, &expr.idx)?;
        let result = match indexable.borrow().get(idx) {
            Ok(val) => Ok(val),
            Err(err_msg) => Err(PopoError::new(expr.bracket.line, err_msg))
        };
        result
    }

    fn visit_lit(&mut self, lit: &Lit) -> Result<Value, PopoError> {
        match lit {
            Lit::Num(num) => Ok(Value::Num(*num)),
            Lit::Str(text) => Ok(Value::Str(text.to_string())),
            Lit::Bool(val) => Ok(Value::Bool(*val)),
            Lit::Nil => Ok(Value::Nil)
        }
    }
}

pub enum ReturnValue {
    Err(PopoError),
    Return {
        keyword: Token,
        value: Value
    }
}

impl From<PopoError> for ReturnValue {
    fn from(err: PopoError) -> Self {
        ReturnValue::Err(err)
    }
}

fn check_bool(val: &Value, token: &Token) -> Result<bool, PopoError> {
    match val {
        Value::Bool(boolean) => Ok(*boolean),
        _ => Err(PopoError::new(
            token.line,
            format!("{} expects boolean, found {}", token.kind, val)
        ))
    }
}

fn check_indexable(val: &Value, token: &Token) -> Result<Rc<RefCell<dyn Indexable>>, PopoError> {
    match val {
        Value::Indexable(indexable) => Ok(indexable.clone()),
        _ => Err(PopoError::new(
            token.line,
            format!("{} is not indexable", val)
        ))
    }
}
