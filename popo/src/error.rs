#[derive(Debug)]
pub struct PopoError {
    pub line: usize,
    pub msg: String
}

impl PopoError {
    pub fn new(line: usize, msg: String) -> Self {
        Self { line, msg }
    }
}

pub fn report_error(error: PopoError) {
    eprintln!("{}", get_error_string(&error))
}

pub fn report_errors(errors: Vec<PopoError>) {
    for error in errors {
        eprintln!("{}", get_error_string(&error))
    }
}

fn get_error_string(error: &PopoError) -> String {
    format!("[ERROR] {}, on line: {}", error.msg, error.line)
}
